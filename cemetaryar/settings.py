"""
Django settings for cemetaryar project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

import os
import sys
import socket
from django.utils.translation import ugettext_lazy as _
from django.conf.global_settings import LOGIN_REDIRECT_URL

PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
BASE_DIR = PROJECT_ROOT

HOSTNAME = socket.gethostname()

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'n)###g*j85$r#fif*qaqh8x(1vl_75!fjw^dk30f7l3mk0xlo5'

TEMPLATE_DEBUG = True

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)
# sys.path could be used here to shorten
# PYTHON_MAGIC_PATH = sys.path + '/magic.py'
PYTHON_MAGIC_PATH = ''


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'


SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = False

LANGUAGES = (
    ('nl-nl', _('Dutch')),
    ('en-gb', _('English')),
)

LOCALE_PATHS = (os.path.join(PROJECT_ROOT, 'locale/'),)

# Make Dutch the default language
LANGUAGE_CODE = 'nl-NL'

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
# If PROJECT_ROOT is '/var/www/uploadering/' then
# MEDIA_ROOT is '/var/www/uploadering/userfiles/'
#
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'userfiles')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
#
MEDIA_URL = '/files/'


# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'cemetaryar.urls'

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAdminUser',),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
    )
}

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'cemetaryar.wsgi.application'

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'channel.context_processors.static_root',
    'channel.context_processors.media_root',
    'channel.context_processors.available_languages',
    'channel.context_processors.single_channel_id',
    'channel.context_processors.is_development'
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'channel',
    'auth',
    'rest_framework',
    'api'
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


# Limit uploads to 5MB
# If alter this - change in apache httpd.conf LimitRequestBody also
MAX_UPLOAD_SIZE = 5242880

ALLOWED_UPLOAD_EXTENSIONS = {
    # Image
    'image/jpeg', 'image/gif', 'image/png',
    # Video - HTML5 only supports Theora(ogg),
    # H.264(MP4), VP8(WebM), VP9(WebM)
    'video/ogg', 'video/mp4', 'video/webm',
    'video/quicktime',  # for iphone captured video
    'application/ogg',
    # Audio - HTML5
    'audio/mpeg', 'audio/ogg',
    'audio/x-wav', 'audio/mp3',
    'audio/webm', 'audio/opus'}

# Set to None if want to allow multiple channels
# otherwise set to specific main channel id
SINGLE_MAIN_CHANNEL_ID = 1

LOGIN_REDIRECT_URL = '/user_visits/'
LOGIN_URL = '/auth/login/'
# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Development servers
if(HOSTNAME == 'sin-VirtualBox' or HOSTNAME == 'sin-Satellite-U300' or HOSTNAME == 'linux-4jq4.site'):
    DEBUG = True
    # With debug set to true static root will work even if no dir set
    STATIC_ROOT = ''
    ALLOWED_HOSTS = []

# Production server
elif(HOSTNAME == 'web433.webfaction.com'):
    # Never set debug to true on live site
    DEBUG = False

    # With debug set to false must specify static root
    STATIC_ROOT = '/home/meschproject/webapps/atlantikwal/mesch-cem/channel/static/'

    # Host names that are valid for this site; required if DEBUG is False
    ALLOWED_HOSTS = ['meschproject.webfactional.com']

else:
    raise AttributeError(HOSTNAME)
    raise AttributeError('Invalid hostname - no settings available')
