from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.template.loader import add_to_builtins
from rest_framework import routers

from django.contrib import admin
admin.autodiscover()
add_to_builtins('django.templatetags.i18n')

urlpatterns = patterns(
    '',
    url(r'^', include('channel.urls', namespace="channel")),
    url(r'^', include('api.urls', namespace="api")),
    url(r'^auth/', include('auth.urls', namespace="auth")),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
    ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
