��    6      �  I   |      �  A   �     �     �     �          "     .     J     Z     k     s     �  1   �  &   �  &   �  #        1     8     D     J     O     e     s     {  N   �     �  0   �       ,        A     J     P     l     �  	   �     �     �     �     �     �  K   �     +     2     B     T  8   [     �     �     �  "   �     �     �     �  c  �  O   a
     �
     �
     �
     �
     �
               *     @     G     Z  +   b  -   �  .   �  #   �            
   (     3     <     X     i     q  !   �     �  >   �     �  ,   �     $     1  *   :     e  	        �     �     �     �  
   �      �  N   �     F     M     a  	   v  7   �     �     �     �     �     �  	                        0   6   4      '                                     +      !   .           5      2      3                 -   /       ,   1   *          $                &   
         (               "   )                              	                #      %    A channel is a collection of locations and media to show at once. Add Location Add Locations Add a new channel Back to Locations Back to Map Boundary Radius (in meters) Centre Latitude Centre Longitude Channel Channel Settings Channels Click here to create a new channel for locations. Click on the map to add a new location Click on the map to select a location. Copyright &copy; meSch Project 2015 Delete Description Dutch Edit Edit Channel Settings Edit Location English Enter passcode Error adding location to map. Please try again resolving the following issues; File File %(type)s not supported. Filetypes allowed:  Home LOCI - Location Oriented Community Interface Latitude Login Login to add a new location Login with passcode Logout Longitude Name No point selected Passcode Login Password Please enter a valid passcode. Please keep filesize under %(max_size)s. Current filesize %(current_size)s. Radius Remove Location Select a channel. Submit This file-type could not be validated for file %(type)s  Title Username View Locations View and edit your added locations description: language code: written by: Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-04-27 07:36+0100
PO-Revision-Date: 2015-03-06 11:00+0100
Last-Translator: Sinead O'Brien <s.obrien@shu.ac.uk>
Language-Team: MeSch <s.obrien@shu.ac.uk>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.4
 Een kanaal is een collectie locaties en  media die gelijktijdig worden getoond. Locatie toevoegen Locaties toevoegen Kanaal toevoegen Terug naar Locaties Terug naar kaart Straal Breedtegraad centreren Lengtegraad centreren Kanaal Kanaalinstellingen Kanalen Klik hier om een nieuw kanaal aan te maken. Klik op de kaart om een locatie toe te voegen Klik op de kaart om een locatie te selecteren, Copyright &copy; meSch Project 2015 Verwijderen Beschrijving Nederlands Bewerken Kanaalinstellingen bewerken Locatie bewerken English Voer je toegangscode in Geen locatie toegevoegd. Oorzaak: Bestand Formaat %(type)s wordt niet ondersteund. Toegestane formaten:  Home LOCI - Location Oriented Community Interface Breedtegraad Inloggen Login om een nieuwe locatie toe te voegen. Inloggen met toegangscode Uitloggen Lengtegraad Naam Geen punt geselecteerd Inloggen met toegangscode Wachtwoord Voer een geldige toegangscode in Bestand moet kleiner zijn dan %(max_size)s. Huidige grootte: %(current_size)s. Straal Locatie verwijderen Selecteer een kanaal Verzenden Dit formaat kan niet worden ge valideerd voor %(type)s  Titel Gebruikersnaam Locaties bekijken Bekijk en bewerk jouw locaties beschrijving: taalcode: geschreven door: 