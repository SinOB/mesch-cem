from rest_framework.decorators import api_view, permission_classes
from api.serializers import UserVisitsSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAdminUser


@api_view(['GET', 'POST'])
@permission_classes((IsAdminUser, ))
def parse_user_visit(request, format=None):
    if request.method == 'GET':
        return Response({'received data': request.data})
    elif request.method == 'POST':
        serializer = UserVisitsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
