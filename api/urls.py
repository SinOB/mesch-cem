from django.conf.urls import patterns, include, url
from rest_framework import routers
from api import views


router = routers.DefaultRouter()
urlpatterns = patterns('',
                       url(r'^', include(router.urls)),
                       url(r'^user_visits', views.parse_user_visit),
                       )
