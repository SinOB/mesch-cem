from channel.models import Location
from django.contrib.auth.models import User
from api.models import Visit


def get_locations(username, channel, locale):
    """Get all locations and assign is_viewed to them based
    on if the user has viewed them"""
    # convert locale to lowercase
    locale = locale.lower()
    all_locations = Location.objects.filter(channel=channel).filter(language_code=locale).order_by('id')

    if username == '':
        # If no username remove fixed locations and return
        locations = all_locations.filter(is_fixed=0).order_by('id')
        return locations

    user = User.objects.get(username=username)
    visited_locations = list(
        Visit.objects.filter(user=user).select_related("location"))

    # For each location
    for location in all_locations:
        # We only care if fixed locations have been viewed
        if location.is_fixed:
            # Default viewed to false
            location.is_viewed = False
            for visit in visited_locations:
                if visit.location.id == location.id:
                    location.is_viewed = True
                    break

    return all_locations
