from django.contrib.auth.models import User
from api.models import Visit
from channel.models import Location
from rest_framework import serializers


class VisitSerializer(serializers.ModelSerializer):

    title = serializers.CharField(max_length=80)

    class Meta:
        model = Visit
        fields = ('date', 'title')


class UserVisitsSerializer(serializers.ModelSerializer):
    language_code = serializers.CharField(max_length=5)
    visits = VisitSerializer(many=True)
    passcode = serializers.CharField(max_length=30)

    class Meta:
        model = User
        fields = ('passcode', 'language_code', 'visits')

    def create(self, validated_data):
        visits_data = validated_data.pop('visits')
        passcode = validated_data.get('passcode')
        language_code = validated_data.get('language_code')

        # If the user already exists don't create again
        if User.objects.filter(username=passcode).exists():
            user = User.objects.get(username=passcode)
        else:
            user = User.objects.create(username=passcode)
            user.set_password(passcode)
            user.save()

        for visit_data in visits_data:

            try:
                location = Location.objects.filter(
                    is_fixed=True).filter(language_code=language_code).get(title=visit_data.get("title"))
            except Exception as e:
                raise serializers.ValidationError('%s %s' % (e, visit_data.get("title")))
            if location:
                # Only store location for user once (don't care about datetime)
                existing = Visit.objects.filter(
                    user=user).filter(location=location)
                # Only create visit if not already existing
                if not existing.exists():
                    vt = Visit()
                    vt.user = user
                    vt.date = visit_data.get("date")
                    vt.location = location
                    vt.save()

        return user
