from django.db import models
from django.contrib.auth.models import User
from channel.models import Location


class Visit(models.Model):
    user = models.ForeignKey(User)
    location = models.ForeignKey(Location)
    date = models.DateTimeField()
