MeSch Cem
=============

About
-----

Mesch-cem is a project to overlay images, sound, and animations on top of a google map. This web app provides an interface to create your own channels, and store locations and media by clicking on an interactive map.

Originally created for use within a cemetary to overlay historical images on top of landmark locations, but can now be used for any purpose to add media to locations on a map.

This project uses Django to create and store data relating to channels.


Requirements
------------

1. Django 1.6.5.

2. python-magic

3. django rest framework


Installation
------------

Mesch-cem is built using django 1.6.5, and acts as a django project.

1. Create a virtualenv for your project, activate it, and install Django 1.6.5 with 'pip install Django==1.6.5'.

2. Move into your new env and clone this project into your workspace.

3. Install python-magic with 'pip install python-magic' - required to safely check uploaded file mimetypes. Ideally if you are running a linux virtualenv everything should just work. If you are using IOS there is a naming conflict with python magic. You will need to make sure you have installed the correct version of python-magic and then set the value PYTHON_MAGIC_PATH to this location the settings file.

4. Install the django rest framework with 'pip install djangorestframework'

5. cd into meSch-cem and run 'python manage.py syncdb' to set up your database and create a superuser.

6. Ensure that the userfiles directory and all newly uploaded files within are owned by www-data (or your own user if using the development server). 'chown -R <your_user>:www-data *'

7. Update cemetaryar/settings.py file with settings for new hostname. Specifically set ALLOWED_HOSTS as appropriate and set STATIC_ROOT = '<route to project>/mesch-cem/channel/static/' and from the command line run 'python manage.py collectstatic' to update static files to correct location

7. Run 'python manage.py runserver' to set up the database, superuser, and launch the development server.



Usage
-----

1. Log in with your user details specified during database creation.

2. Add a new channel - each channel will store locations and media.

3. Click on the map and provide a title and media to store the location to the database and create the files on the server.

4. Fullscreen (read-only) view is available to non logged in users from url
<domain_root>/channel/<channel_id>/?fullscreen=1

API
-----
Only accessable to user accounts with is_staff set to true.

1. Login url is <domain_root>/api-auth/login/

2. Update User Visits is <domain_root>/user_visits/

3. To go directly from Login to Uservisits after successful login use <domain_root>/api-auth/login/?next=/user_visits/

Expecting json input in the format:

{    
	"passcode":"Uppercase string under 30 characters",
	"language_code": "5 char lowercase string. Current options are nl-nl or en-gb",
    "visits":[
        {"date":"2015-03-10 09:22:51.156654","title":"existing fixed title string max 80"},
        {"date":"2015-02-01 09:22:51.156654","title":"existing fixed title string max 80"}]
}


Troubleshooting
---------------

#### The map is not updating when I add a location / a javascript error is present

This happens when the directory located at CHANNEL_ROOT does not have sufficient permissions to be written to by the site. Try using `chmod -R` on the channels directory to give it write access, or ensure that it is owned by either your user account for the development server or www-data if deployed.

#### The channel / location / user is not being added or created 

Most issues with this project will come from not having the correct permissions or ownership on your files, ensure that both the database file and the channels within your channels directory have read and write access, and if you have deployed the website then it helps if they are owned by www-data. 

##### System crashes when try to upload movie file directly from phone

This and other issues(python magic package import) appear to be related to trying to run an IOS virtual env. Easiest way to work around is to
switch virtual environment to be linux based. 

Deployment
----------

To run outside of the django development server please follow the Django documentation for deploying using Apache and WSGI or other web servers.

https://docs.djangoproject.com/en/dev/howto/deployment/wsgi/modwsgi/
https://docs.djangoproject.com/en/dev/howto/deployment/
