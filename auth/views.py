from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.utils.translation import ugettext_lazy as _
import sys


# Login based on a single field - accesscode
def auth_passcode_login(request):
        # Redirect to home if user is already logged in
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    else:
        error = ""
        if request.method == 'POST':
            # Use the authentication form for the user and pass
            # Automatically convert to uppercase as don't trust user
            request.POST['username'] = request.POST['username'].upper()
            request.POST['password'] = request.POST['username']
            form = AuthenticationForm(data=request.POST)
            if form.is_valid():
                username = request.POST['username']
                password = request.POST['password']
                user = authenticate(username=username, password=password)
                if user is not None:
                    login(request, user)
                    return HttpResponseRedirect('/')
        else:
            form = AuthenticationForm()

    form.fields['password'].widget.attrs['placeholder'] = _("Enter passcode")
    context = {
        'form': form,
        'error': error,
        }
    return render(request, 'auth/passcode.html', context)


# Login based on standard username and password
def auth_login(request):

    # Redirect to home if user is already logged in
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    else:
        error = ""
        if request.method == 'POST':
            # Use the authentication form for the user and pass
            form = AuthenticationForm(data=request.POST)
            if form.is_valid():
                username = request.POST['username']
                password = request.POST['password']
                user = authenticate(username=username, password=password)

                # If authentication returns a user then log them in
                if user is not None:
                    login(request, user)
                    return HttpResponseRedirect('/')

        else:
            form = AuthenticationForm()

        # Set the additional properties of the django login form
        form.fields['username'].widget.attrs['placeholder'] = _("Username")
        form.fields['password'].widget.attrs['placeholder'] = _("Password")

        context = {
            'form': form,
            'error': error,
        }
        return render(request, 'auth/login.html', context)


def auth_logout(request):

    logout(request)
    return HttpResponseRedirect('/')
