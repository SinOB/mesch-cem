from django.conf.urls import patterns, url

from auth.views import auth_login, auth_logout, auth_passcode_login

urlpatterns = patterns('',
                       url(r'^login/$', auth_login, name='login'),
                       url(r'^passcode/$',
                           auth_passcode_login,
                           name='passcode'),
                       url(r'^logout/$', auth_logout, name='logout'))
