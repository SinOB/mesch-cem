from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.dispatch import receiver
from auth.models import LoginLog, LogoutLog
from django.utils import timezone
import datetime


@receiver(user_logged_in)
def sig_user_logged_in(sender, user, request, **kwargs):
    date = timezone.make_aware(datetime.datetime.today(),
                               timezone.get_current_timezone())
    log = LoginLog(user=user, date=date)
    log.save()


@receiver(user_logged_out)
def sig_user_logged_out(sender, user, request, **kwargs):
    date = timezone.make_aware(datetime.datetime.today(),
                               timezone.get_current_timezone())
    log = LogoutLog(user=user, date=date)
    log.save()
