from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class LoginLog(models.Model):
    date = models.DateTimeField()
    user = models.ForeignKey(User)


class LogoutLog(models.Model):
    date = models.DateTimeField()
    user = models.ForeignKey(User)
