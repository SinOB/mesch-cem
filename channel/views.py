import hashlib
import logging
import sys
import os
import datetime
from itertools import chain

from django.shortcuts import render, render_to_response
from django.http import Http404, HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404, redirect
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.generic import FormView, UpdateView
from django.utils.decorators import method_decorator
from django.utils import timezone

from channel.models import Channel, Location, LocationLog
from channel.forms import ChannelForm, LocationForm

from api.utils import get_locations

from django.utils import translation


# If the user is authenticated list all channels and provide add new channel
# form. If the form is POSTed then add the new channel to the database
# (if no errors).
def index(request):
    # If only one channel allowed - default to this
    if(settings.SINGLE_MAIN_CHANNEL_ID):
        return HttpResponseRedirect('/channel/%s/'
                                    % settings.SINGLE_MAIN_CHANNEL_ID)

    if request.method == 'POST':
        form = ChannelForm(request.POST)
        if form.is_valid():
            new_channel = form.save()
            return HttpResponseRedirect('/')
        else:
            all_channels = Channel.objects.all().order_by('title')
            context = {
                'form': form,
                'channels': all_channels,
            }
    else:
        all_channels = Channel.objects.all().order_by('title')
        form = ChannelForm()
        context = {
            'channels': all_channels,
            'form': form,
        }

    return render(request, 'channel/channel.html', context)


# Load the channel and query all (non-fixed) locations associated
# with that channel to pass to template. Pass a form to add new locations.
def detail(request, channel_id):
    try:
        channel = Channel.objects.get(pk=channel_id)
        locations = get_locations(request.user.username,
                                  channel,
                                  request.LANGUAGE_CODE)

        # Only allow fullscreen for logged out users
        fullscreen = None
        if request.GET.get('fullscreen') == '1' and \
                not request.user.is_authenticated():
            fullscreen = request.GET.get('fullscreen', '')
            request.session['django_language'] = 'nl-nl'

        form = LocationForm()
        context = {
            'locations': locations,
            'channel': channel,
            'form': form,
            'fullscreen': fullscreen
        }
    except Exception as e:
        raise

    return render(request, 'channel/detail.html', context)


# Query all locations for the channel to list in the template.
@login_required
def view_locations(request, channel_id):
    channel = Channel.objects.get(pk=channel_id)

    if request.user.is_staff:
        # On edit page view all non fixed locations
        locations = Location.objects.filter(channel=channel)
        locations = locations.filter(is_fixed=0).order_by('-id')
        if locations:
            context = {
                'locations': locations,
                'channel': channel,
            }

        return render(request, 'channel/view_locations.html', context)

    # User is not staff or have no locations
    return HttpResponseRedirect('/channel/%s/' % channel.id)


# Refresh the map using the channel, lat and long passed in to
# determine the center.
def update_map(request, channel_id, latitude, longitude, radius):
    channel = Channel.objects.get(pk=channel_id)
    locations = get_locations(request.user.username,
                              channel,
                              request.LANGUAGE_CODE)

    context = {
        'locations': locations,
        'channel': channel,
        'latitude': latitude,
        'longitude': longitude,
        'radius': radius,
    }

    return render(request, 'channel/map.html', context)


class AddLocation(FormView):
    form_class = LocationForm
    template_name = 'channel/location_form.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AddLocation, self).dispatch(*args, **kwargs)

    # handle case where have submitted a form
    # gotten an error, the error is shown with the map detail below
    # and for some reason the user refreshes the page ..
    # redirect back to detail page with no form and no form error messages
    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context = self.get_context_data(**kwargs)
        context['form'] = form
        if not request.GET.get('get') == '1':
            channel_id = self.kwargs.get('channel_id')
            return redirect('/channel/%s/' % channel_id)
        return self.render_to_response(context)

    def get_success_url(self, *args, **kwargs):
        channel_id = self.kwargs.get('channel_id')
        return '/channel/%s/' % channel_id

    def form_valid(self, form, **kwargs):
        channel_id = self.kwargs.get('channel_id')
        channel = Channel.objects.get(pk=channel_id)
        # If we don't have a file we can't have an md5 of file
        if form.files.get('file'):
            hash_value = hashlib.md5(form.files.get('file').read()).hexdigest()
            file_type = form.files.get('file').content_type
        else:
            hash_value = None
            file_type = None
        new_location = form.save(commit=False)
        new_location.channel = Channel.objects.get(pk=channel_id)
        new_location.md5 = hash_value
        new_location.file_type = file_type
        new_location.updated_by = self.request.user
        new_location.language_code = self.request.LANGUAGE_CODE.lower()
        new_location.save()
        form.save_m2m()

        # Save the creation to the database log
        log = LocationLog(user=self.request.user,
                          location_id=new_location.id,
                          type='created')
        log.save()

        return super(AddLocation, self).form_valid(form)

    def form_invalid(self, form):
        """
        If the form is invalid, re-render the context data with the
        data-filled form and errors.
        """
        channel_id = self.kwargs.get('channel_id')
        channel = Channel.objects.get(pk=channel_id)
        locations = get_locations(self.request.user.username,
                                  channel,
                                  self.request.LANGUAGE_CODE)
        context = {
            'locations': locations,
            'channel': channel,
            'form': form,
        }
        return render(self.request, 'channel/detail.html', context)


# Edit a location, pass in a form instance of the requested location to
# make changes to.
class EditLocation(UpdateView):
    form_class = LocationForm
    model = Location
    template_name = 'channel/edit_location.html'

    def get_object(self, queryset=None):
        obj = get_object_or_404(Location, pk=self.kwargs['location_id'])
        return obj

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        location_id = self.kwargs.get('location_id')
        location = Location.objects.get(pk=location_id)
        # If user is staff or last updated the location allow them to edit
        if request.user.is_staff or location.updated_by == request.user:
            return super(EditLocation, self).dispatch(request, *args, **kwargs)

        return HttpResponseForbidden()

    def get_success_url(self, *args, **kwargs):
        channel_id = self.kwargs.get('channel_id')
        return '/channel/%s/' % channel_id

    def get_context_data(self, **kwargs):
        channel_id = self.kwargs.get('channel_id')
        context = super(EditLocation, self).get_context_data(**kwargs)
        context['channel'] = Channel.objects.get(pk=channel_id)
        return context

    def form_valid(self, form):
        location = form.instance

        # if have a new file reset the md5 and file type
        # otherwise handled automagically
        if form.files.get('file'):
            hash_value = hashlib.md5(form.files.get('file').read()).hexdigest()
            file_type = form.files.get('file').content_type
            location.md5 = hash_value
            location.file_type = file_type

        elif 'file-clear' in self.request.POST:
            location.md5 = None
            location.file_type = None

        location.updated_by = self.request.user
        location.updated = timezone.make_aware(
            datetime.datetime.today(),
            timezone.get_current_timezone())
        location.save()

        # Save the deletion to the database log
        log = LocationLog(user=self.request.user,
                          location_id=location.id,
                          type='edited')
        log.save()

        return super(EditLocation, self).form_valid(form)


# Remove a location from the db.
@login_required
def remove_location(request, channel_id, location_id):
    channel = Channel.objects.get(pk=channel_id)
    location = get_object_or_404(Location, pk=location_id)

    # Save the deletion to the database log
    log = LocationLog(user=request.user,
                      location_id=location.id,
                      type='deleted')
    log.save()

    location.delete()
    # to do - make sure delete the actual file in the directory at this point
    return HttpResponse('successful delete')


# Edit the channel settings, only staff can use these.
# Updates the channel when a change is made.
@staff_member_required
@login_required
def edit_channel(request, channel_id):
    channel = Channel.objects.get(pk=channel_id)
    if request.method == 'POST':
        form = ChannelForm(request.POST, instance=channel)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/channel/%s/' % channel_id)
    else:
        form = ChannelForm(instance=channel)
    context = {
        'form': form,
        'channel': channel,
    }

    return render(request, 'channel/channel_settings.html', context)


# Not yet implemented, remove a channel without the django admin.
@staff_member_required
@login_required
def remove_channel(request, channel_id):
    channel = get_object_or_404(Channel, pk=channel_id)
    channel.delete()
    return HttpResponse('successful delete')
