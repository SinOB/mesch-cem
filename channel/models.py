from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
import sys
import os
from django.conf import settings
from django.dispatch import receiver
from django.template import defaultfilters
from django.utils.translation import ugettext_lazy as _
import datetime
from django.utils import timezone


# Apparently there is a naming conflict with magic in IOS which the
# package owners are both too stubborn to fix - so attempt work around
try:
    import magic
    if not hasattr(magic, 'from_buffer'):
        import imp
        magic = imp.load_source('magic', settings.PYTHON_MAGIC_PATH)
except ImportError:
    print >> sys.stderr, "No magic module available."


# The FileField content-type is supplied by browser therefore not a
# reliable way to get the mimetime. We must therefore check the file
# using magic. 'trust but verify'
def validate_mime(value):
    try:
        mime = magic.from_buffer(value.read(1024), mime=True)
        if mime not in settings.ALLOWED_UPLOAD_EXTENSIONS:
            raise ValidationError(
                _('File %(type)s not supported. Filetypes allowed: ')
                % {'type': value} +
                ', ' .
                join(settings.ALLOWED_UPLOAD_EXTENSIONS))
    except AttributeError as e:
        raise ValidationError(
            _('This file-type could not be validated for file %(type)s '
              % {'type': value}))


def validate_size(value):
    try:
        if value.size > settings.MAX_UPLOAD_SIZE:
                raise ValidationError(
                    _('Please keep filesize under %(max_size)s. Current filesize %(current_size)s.')
                    % {'max_size': defaultfilters.filesizeformat(settings.MAX_UPLOAD_SIZE),
                       'current_size': defaultfilters.filesizeformat(value.size)})
    except AttributeError as e:
        raise ValidationError(
            _('Please keep filesize under %(max_size)s. Current filesize %(current_size)s.')
            % {'max_size': defaultfilters.filesizeformat(settings.MAX_UPLOAD_SIZE),
                'current_size': defaultfilters.filesizeformat(value.size)})


class Channel(models.Model):

    title = models.CharField(_("Title"), max_length=20)
    description = models.CharField(_("Description"),
                                   max_length=140,
                                   blank=True)
    centre_latitude = models.FloatField(_("Latitude"))
    centre_longitude = models.FloatField(_("Longitude"))
    radius = models.IntegerField(_("Radius"), blank=True, null=True)

    def __unicode__(self):
        return self.title


class Location(models.Model):
    file = models.FileField(upload_to='.',
                            validators=[validate_mime, validate_size],
                            blank=True, null=True)
    title = models.CharField(_("Title"), max_length=80)
    latitude = models.FloatField(_("Latitude"))
    longitude = models.FloatField(_("Longitude"))
    description = models.TextField(_("Description"), max_length=250)
    name = models.CharField(_("Description"), max_length=150, blank=True, null=True)
    channel = models.ForeignKey(Channel,
                                blank=True,
                                null=True,
                                verbose_name=_('Channel'))
    md5 = models.CharField(max_length=32, blank=True, null=True)
    file_type = models.CharField(max_length=32, blank=True, null=True)
    is_fixed = models.BooleanField(default=False)
    language_code = models.CharField(max_length=5,
                                     default=settings.LANGUAGE_CODE.lower(),
                                     choices=settings.LANGUAGES)

    updated = models.DateTimeField(
        default=timezone.make_aware(datetime.datetime.today(),
                                    timezone.get_current_timezone()))
    updated_by = models.ForeignKey(User)

    def __unicode__(self):
        return self.title


# Auto-delete files from filesystem when Location deleted from database
@receiver(models.signals.post_delete, sender=Location)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding 'Location' object is deleted.
    """
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)


# Auto-delete files from filesystem when Location file changed
@receiver(models.signals.pre_save, sender=Location)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `Location` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file = Location.objects.get(pk=instance.pk).file
    except Location.DoesNotExist:
        return False

    new_file = instance.file
    if old_file and not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)
        return False


class LocationLog(models.Model):
    LOGTYPES = (
        ('c', 'created'),
        ('e', 'edited'),
        ('d', 'deleted'),
    )

    date = models.DateTimeField(
        default=timezone.make_aware(datetime.datetime.today(),
                                    timezone.get_current_timezone()))
    user = models.ForeignKey(User)
    # Do not use foreign key to store Location as system will
    # delete LogLocation when Location object is deleted
    location_id = models.IntegerField()
    type = models.CharField(max_length=1, choices=LOGTYPES)
