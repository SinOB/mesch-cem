from django.conf.urls import patterns, url

from channel.views import AddLocation, EditLocation
from channel import views


urlpatterns = patterns(
    '',
    url(r'^channel/(?P<channel_id>\d+)/add/$',
        AddLocation.as_view(), name='add_location'),
    url(r'^$', views.index, name='index'),
    url(r'^channel/(?P<channel_id>\d+)/$',
        views.detail, name='detail'),
    url(r'^channel/(?P<channel_id>\d+)/view/$',
        views.view_locations, name='view'),
    url(r'^channel/(?P<channel_id>\d+)/remove/(?P<location_id>\d+)/$',
        views.remove_location, name='remove_location'),
    url(r'^channel/(?P<channel_id>\d+)/edit/(?P<location_id>\d+)/$',
        EditLocation.as_view(), name='edit_location'),
    url(r'^channel/(?P<channel_id>\d+)/edit/$',
        views.edit_channel, name='edit_channel'),
    url(r'^channel/(?P<channel_id>\d+)/map/(?P<latitude>(-?\d+\.\d+)),(?P<longitude>(-?\d+\.\d+)),(?P<radius>(-?\d+))/$',
        views.update_map, name='update_map'),)
