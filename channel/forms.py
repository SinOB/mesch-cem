from django.forms import ModelForm
from django import forms
from channel.models import Channel, Location
from django.utils.translation import ugettext_lazy as _
import sys


class ChannelForm(ModelForm):
    class Meta:
        model = Channel
        widgets = {
            'title': forms.TextInput(attrs={'placeholder': 'Title'}),
            'description': forms.TextInput(attrs={
                'placeholder': _('Description')}),
            'centre_latitude': forms.NumberInput(attrs={
                'placeholder': _('Centre Latitude')}),
            'centre_longitude': forms.NumberInput(attrs={
                'placeholder': _('Centre Longitude')}),
            'radius': forms.NumberInput(attrs={
                'placeholder': _('Boundary Radius (in meters)')}),
        }


class LocationForm(forms.ModelForm):
    """Upload files with this form"""
    class Meta:
        model = Location
        exclude = ('md5', 'file_type', 'updated', 'updated_by', 'is_fixed')
        widgets = {'title': forms.TextInput(attrs={'placeholder': _('Title')}),
                   'description': forms.Textarea(
                       attrs={'placeholder': _('Description')}),
                   'name': forms.TextInput(attrs={'placeholder': _('Name')})}
