# /uploadering/filebaby/context_processors.py

from django.conf import settings


def static_root(request):
    """
    Adds static-root context variables to the context.

    """
    return {'STATIC_ROOT': settings.STATIC_ROOT}


def media_root(request):
    """
    Adds media-root context variables to the context.

    """
    return {'MEDIA_ROOT': settings.MEDIA_ROOT}


def available_languages(request):
    return {'AVAILABLE_LANGUAGES': settings.LANGUAGES}


def single_channel_id(request):
    return {'SINGLE_MAIN_CHANNEL_ID': settings.SINGLE_MAIN_CHANNEL_ID}


def is_development(request):
    return {'IS_DEVELOPMENT': settings.DEBUG}
